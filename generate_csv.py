#!/usr/bin/env python3
import sys
import hvac
import os
import csv
import hvac
import requests
from requests.adapters import HTTPAdapter


def main():

    # Vault connection with local token, or k8s auth
    vault = hvac.Client(url=os.environ.get('VAULT_ADDR'))
    if 'VAULT_TOKEN' in os.environ:
        sys.stderr.write("Using VAULT_TOKEN auth\n")
        vault.token = os.environ['VAULT_TOKEN']
    elif 'VAULT_OKD' in os.environ:
        sys.stderr.write("Using K8s auth")
        f = open('/var/run/secrets/kubernetes.io/serviceaccount/token')
        jwt = f.read()
        vault.auth_kubernetes("splunk-ap-buildings",
                              jwt,
                              mount_point=os.environ.get('VAULT_OKD'))

    cartographer_auth = vault.read(
        'secret/shared_service_accounts/cartographer-ssi')

    cartographer_url = "https://cartographer.oit.duke.edu"
    s = requests.Session()
    s.mount(cartographer_url, HTTPAdapter(max_retries=5))

    resp = requests.get('%s/api/v1/access_points/with_buildings.json' %
                        cartographer_url,
                        auth=(cartographer_auth['data']['username'],
                              cartographer_auth['data']['api_key']))
    resp.raise_for_status()
    expected_fields = [
        'ap_group', 'building_id', 'building_name', 'ethernet_mac', 'ip',
        'map_location', 'model', 'name', 'radio_mac', 'type'
    ]

    data = resp.json()
    with open('tmp/data.csv', 'w') as csvfile:
        writer = csv.DictWriter(csvfile,
                                delimiter=',',
                                fieldnames=expected_fields)
        writer.writeheader()
        for item in (data):
            res = dict((k, item[k]) for k in expected_fields if k in item)
            writer.writerow(res)
    return 0


if __name__ == "__main__":
    sys.exit(main())
