# ITSO Splunk Buildings AP Report

This is a script that runs to generate a CSV of building aps, and copy it to
the splunk search heads

The job runs as a kubernetes cron job.  Full information at:

```
oc get cronjob splunk-ap-buildings-run -o yaml -n splunk-ap-buildings
```

If you need to run it as a one-off, you can do:

```
oc create job test-run-$(date +%s) --from=cronjob/splunk-ap-buildings-run -n splunk-ap-buildings
```
