#!/usr/bin/env bash

set -x
set -e

if ! whoami &> /dev/null; then
  if [ -w /etc/passwd ]; then
    echo "${USER_NAME:-default}:x:$(id -u):0:${USER_NAME:-default} user:/code/:/sbin/nologin" >> /etc/passwd
  fi
fi

cd /code/
mkdir -p tmp/

./generate_csv.py

./generate_ssh_key.py

ansible-playbook ./playbook.yaml
