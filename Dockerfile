FROM python:3
RUN mkdir /code && \
    mkdir /.ansible

COPY . /code/

WORKDIR /code
RUN pip install -r requirements.txt

RUN chgrp -R 0 /code/ && \
    chmod -R g=u /code/ && \
    chgrp -R 0 /.ansible && \
    chmod -R g=u /.ansible && \
    chmod g=u /etc/passwd


ENTRYPOINT ["/code/run.sh"]
