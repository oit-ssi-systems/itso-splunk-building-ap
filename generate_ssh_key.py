#!/usr/bin/env python3
import sys
import hvac
import os
import hvac
import subprocess


def main():

    # Vault connection with local token, or k8s auth
    vault = hvac.Client(url=os.environ.get('VAULT_ADDR'))
    if 'VAULT_TOKEN' in os.environ:
        sys.stderr.write("Using VAULT_TOKEN auth\n")
        vault.token = os.environ['VAULT_TOKEN']
    elif 'VAULT_OKD' in os.environ:
        sys.stderr.write("Using K8s auth")
        f = open('/var/run/secrets/kubernetes.io/serviceaccount/token')
        jwt = f.read()
        vault.auth_kubernetes("splunk-ap-buildings",
                              jwt,
                              mount_point=os.environ.get('VAULT_OKD'))

    if not os.path.exists("tmp/id_rsa"):
        print("Creating rsa key")
        subprocess.check_call(
            ["ssh-keygen", "-t", "rsa", "-N", "", "-f", "tmp/id_rsa"])

    with open('tmp/id_rsa.pub', 'r') as rsa_pub:
        pub_key = rsa_pub.read()
        res = vault.write('ssh-client-signer/sign/splunk',
                          public_key=pub_key)['data']['signed_key']
        with open('tmp/id_rsa.pub.signed', 'w') as rsa_pub_signed:
            rsa_pub_signed.write(res)

    return 0


if __name__ == "__main__":
    sys.exit(main())
